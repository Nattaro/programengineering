﻿using ECM7.Migrator.Framework;
using System;

namespace Migrations._2018._09.Version_2018091610000
{
    [Migration(2018091610000)]
    public class UpdateSchema : UserMigration
    {
        public override void Apply()
        {
            Database.AddTable(new SchemaQualifiedObjectName() { Schema = "public", Name = "TestMigration" }, new Column[] {
            new Column("test1", new ColumnType(System.Data.DbType.Int32,10),ColumnProperty.PrimaryKey),
            new Column("test2",new ColumnType(System.Data.DbType.Date),ColumnProperty.NotNull,"now()"),
            new Column("test3",new ColumnType(System.Data.DbType.StringFixedLength),ColumnProperty.Unique)});
        }
    }
}
