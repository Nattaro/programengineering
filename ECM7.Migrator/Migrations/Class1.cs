﻿using ECM7.Migrator;
using ECM7.Migrator.Providers;
using ECM7.Migrator.Providers.PostgreSQL;
using System;
using System.Reflection;

namespace Migrations
{
    public class Class
    {
        public bool Load(string connectionString, string regim)
        {
            var type = typeof(PostgreSQLTransformationProvider);
            var provider = ProviderFactory.Create(type, connectionString);
            Assembly asm = Assembly.LoadFrom("Migrations.dll");
            Migrator migrator = new Migrator(provider, asm);
            try
            {
                switch (regim)
                {
                    case "\\up":
                        {
                            migrator.Migrate();
                        }
                        break;
                    case "\\down":
                        {
                            migrator.Migrate(1);
                        }
                        break;
                    case "exit":
                        {
                            return true;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Неизвестная команда");
                            return false;
                        }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }
    }
}
