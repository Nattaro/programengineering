﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECM7.Migrator.Framework;

namespace Migrations
{
    abstract public class UserMigration : Migration
    {
        public void AddColumnIfNotExists(SchemaQualifiedObjectName table, Column column)
        {
            if (!Database.ColumnExists(table, column.Name))
            {
                Database.AddColumn(table, column);
            }
        }

        public void AddIndexIfNotExists(string name, bool unique, SchemaQualifiedObjectName table, params string[] columns)
        {
            if (!Database.IndexExists(name, table))
            {
                Database.AddIndex(name, unique, table, columns);
            }
        }

        public void AddForeignKeyIfNotExists(
﻿  ﻿  ﻿     string name,
﻿  ﻿  ﻿     SchemaQualifiedObjectName primaryTable,
﻿  ﻿  ﻿     string primaryColumn,
﻿  ﻿  ﻿     SchemaQualifiedObjectName refTable,
﻿  ﻿  ﻿     string refColumn)
        {
            if (!Database.ConstraintExists(primaryTable, name))
            {
                Database.AddForeignKey(name, primaryTable, new string[] { primaryColumn }, refTable, new string[] { refColumn });
            }
        }
    }
}
