﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseMigration
{
    public class ConfigReader
    {
        public static Dictionary<string, string> GetValuesFromConfig(string PathToConfig)
        {
            Dictionary<string, string> Result = new Dictionary<string, string>();
            try
            {

                var conf = GetNameValueCollectionSection("appSettings", PathToConfig);

                for (int i = 0; i < conf.Count; i++)
                {
                    if (conf.Keys[i] == "DataBase")
                    {
                        Result.Add("ResultString", conf[i]);
                    }
                    if (conf.Keys[i] == "DrdaPort")
                    {
                        Result.Add("Port", conf[i]);
                    }
                    if (conf.Keys[i] == "ipString")
                    {
                        Result.Add("ipString", conf[i]);
                    }
                }

            }
            catch (Exception e)
            {
                EventLog.WriteEntry("CalcSource", "Ошибка чтения конфиг файла" + e.Message, EventLogEntryType.Information);
            }
            return Result;
        }

        public static NameValueCollection GetNameValueCollectionSection(string section, string filePath)
        {
            string file = filePath;
            System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
            NameValueCollection nameValueColl = new NameValueCollection();

            ExeConfigurationFileMap map = new ExeConfigurationFileMap();
            map.ExeConfigFilename = file;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            string xml = config.GetSection(section).SectionInformation.GetRawXml();
            xDoc.LoadXml(xml);

            System.Xml.XmlNode xList = xDoc.ChildNodes[0];
            foreach (System.Xml.XmlNode xNodo in xList)
            {
                nameValueColl.Add(xNodo.Attributes[0].Value, xNodo.Attributes[1].Value);

            }

            return nameValueColl;
        }
    }
}
