﻿using Migrations;
using System;

namespace DataBaseMigration
{
    class Program
    {
        static void Main(string[] args)
        {
          
            var configs = ConfigReader.GetValuesFromConfig("");
            Class c = new Class();
            while (true)
            {
                Console.WriteLine("\\up - to last version, \\down -revert version, exit - from exit");
                if (c.Load(configs["ResultString"], Console.ReadLine())) return;
            }
        }
    }
}
