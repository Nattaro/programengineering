﻿/** Процедура выполнения Ajax-запроса и проверка результата
url - Адрес контроллера
method - метод ( GET или POST)
params - параметры
successFunction - функция, которая будет вызвана при успешном завершении запроса, в параметре передает полученный с сервера объект
failureFunction - функция, которая будет вызвана при неудачном завершении запроса (сообщение об ошибке появится автоматически). Если есть объект с сервера, то он передается в параметре.
errorTitle - заголовок сообщение об ошибке (когда resultCode == 0)
*/
Ext.execAjaxRequest = function (url, method, params, successFunction, failureFunction, errorTitle, fn) {
    if (Ext.allRequestAborted) return;
    if (!url) {
        if (typeof (failureFunction) == 'function') failureFunction();
        return;
    }
    if (!method) method = 'GET';
    if (!errorTitle) errorTitle = '';
    fail = function (serverResponse) {
        if (Ext.allRequestAborted) return;
        Ext.MessageBox.show({
            title: 'Ошибка запроса',
            msg: serverResponse.status + ' ' + serverResponse.statusText,
            width: 250,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR,
            fn: fn
        });
        if (typeof (failureFunction) == 'function') failureFunction(serverResponse);
    }
    succ = function (serverResponse) {
        if (Ext.allRequestAborted) return;
        var jsonResp = Ext.decode(serverResponse.responseText);
        if (jsonResp.resultCode == -9998) {
            if (!fn) fn = showLogin;
        }
        if (jsonResp.resultCode >= 0) {
            if (typeof (successFunction) == 'function') successFunction(jsonResp);
        }
        else {
            Ext.MessageBox.show({
                title: errorTitle,
                msg: jsonResp.resultMessage,
                width: 400,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                fn: fn
            });
            if (typeof (failureFunction) == 'function') failureFunction(jsonResp);
        }
    }
    if (method === 'GET') {
        Ext.Ajax.request({
            url: url,
            method: 'GET',
            params: params,
            success: succ,
            failure: fail
        });
    } else {
        Ext.Ajax.request({
            url: url,
            method: 'POST',
            jsonData: params,
            success: succ,
            failure: fail,
            timeout: 60 * 60 * 1000
        });
    }
}

/** Процедура выполнения Ajax-запроса и проверка результата
store - store)
params - параметры
successFunction - функция, которая будет вызвана при успешном завершении запроса, в параметре передает полученные с сервера строки
failureFunction - функция, которая будет вызвана при неудачном завершении запроса (сообщение об ошибке появится автоматически). В параметре передает полученные с сервера строки и информацию о запросе
errorTitle - заголовок сообщение об ошибке (когда success == 0)
*/
Ext.execStoreLoad = function (store, params, successFunction, failureFunction, errorTitle, fn) {
    if (!store) {
        if (failureFunction) failureFunction();
        return;
    }

    store.load({
        params: params,
        callback: function (records, operation, success, eOpts) {
            if (!success) {
                Ext.MessageBox.show({
                    title: 'Ошибка запроса',
                    msg: operation.error.status + ' ' + operation.error.statusText,
                    width: 400,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    fn: fn
                });
                if (typeof (failureFunction) == 'function') failureFunction(records, operation);
            }
            else {
                jsonResp = Ext.decode(operation.response.responseText);
                if (jsonResp.resultCode == -9998) {
                    if (!fn) fn = showLogin;
                }
                if (jsonResp.resultCode < 0) {
                    Ext.MessageBox.show({
                        title: errorTitle,
                        msg: jsonResp.resultMessage,
                        width: 400,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        fn: fn
                    });
                    if (typeof (failureFunction) == 'function') failureFunction(records, operation);
                }
                else {
                    if (typeof (successFunction) == 'function') successFunction(records);
                }
            }
        }
    });
}

function showLogin() {
    SUBS.utils.DesktopManager.closeAll();
}