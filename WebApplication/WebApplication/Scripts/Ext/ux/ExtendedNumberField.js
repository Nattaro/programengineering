﻿Ext.define('Ext.ux.ExtendedNumberField', {
    extend: 'Ext.form.NumberField',
    alias: 'widget.extendednumberfield',

    decimalPrecision: 3,
    baseChars: "-0123456789,.",
    setValue: function (v) {
        v = String(v).replace(" ", "").replace(/ /g, "");
        v = typeof v == 'number' ? v : String(v).replace(",", ".").replace(/,/g, "");
        var parseString = '000';
        if (this.decimalPrecision > 0) {
            parseString = + '.';

            for (var i = 0; i < this.decimalPrecision; i++)
                parseString = + '0';
        }
        v = Ext.util.Format.number(this.fixPrecision(String(v)), parseString);

        this.setRawValue(this.fixPrecision(v));
        return Ext.form.NumberField.superclass.setValue.call(this, v);
    },
    fixPrecision: function (value) {
        var nan = isNaN(value);
        if (!this.allowDecimals || this.decimalPrecision == -1 || nan || !value) {
            return nan ? '' : value;
        }
        return parseFloat(value).toFixed(this.decimalPrecision);
    },
    validateValue: function (value) {
        value = String(value).replace(" ", "").replace(/ /g, "");
        value = String(value).replace(",", ".").replace(/,/g, "");

        if (!Ext.form.NumberField.superclass.validateValue.call(this, value)) {
            return false;
        }

        if (value.length < 1) {
            return true;
        }

        if (isNaN(value)) {
            this.markInvalid(String.format(this.nanText, value));
            return false;
        }

        var num = this.parseValue(value);

        if (num < this.minValue) {
            this.markInvalid(String.format(this.minText, this.minValue));
            return false;
        }

        if (num > this.maxValue) {
            this.markInvalid(String.format(this.maxText, this.maxValue));
            return false;
        }

        return true;
    },
    parseValue: function (value) {
        value = String(value).replace(" ", "").replace(/ /g, "");
        value = parseFloat(String(value).replace(",", ".").replace(/,/g, ""));
        return isNaN(value) ? '' : value;
    }
});