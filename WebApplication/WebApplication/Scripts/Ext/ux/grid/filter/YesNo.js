﻿Ext.define('Ext.ux.grid.filter.YesNo', {
    extend: 'Ext.form.ComboBox',
    requires: ['Ext.form.field.ComboBox'],
    alias: ['widget.gridfilteryesno'],
    alternateClassName: 'Ext.ux.grid.Filter.YesNo',

    hideLabel: true,
    editable: false,
    valueField: 'value',
    displayField: 'name',
    queryMode: 'local',
    triggerAction: 'all',
    operand: CondExpr.operands.eq,
    constructor: function (config) {
        config.store = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data: [
                { 'value': null, 'name': '-' },
                { 'value': 1, 'name': 'Да' },
                { 'value': 0, 'name': 'Нет' }
            ]
        });

        this.callParent([config]);
    },
    listeners: {
        afterrender: {
            fn: function (me) {
                me.select(me.getStore().data.items[0]);
            },
            single: true
        }
    }
});