﻿Ext.define('Ext.ux.grid.SelectGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.selectgrid',
    alternateClassName: ['Ext.grid.SelectGrid'],

    idProperty: 'Id',

    destroy: function () {
        var me = this;
        me.selModel.destroy();
        me.callParent(arguments);
    },

    /*
    * Показывать кнопку "Выбрать все"
    */
    showSelectAll: false,

    createSelModel: function () {
        var grid = this;
        var idProperty = grid.idProperty;
        var store = grid.getStore();

        var selModel = Ext.create('Ext.selection.CheckboxModel', {
            mode: 'MULTI',
            checkOnly: true,
            multipageSelection: [],
            listeners: {
                selectionchange: function (selectionModel, selectedRecords) {
                    var me = this;
                    
                    if (store.loading == true) {
                        me.multipageSelection = [];
                        return;
                    }

                    store.data.each(function (record) {
                        Ext.Array.remove(me.multipageSelection, record.get(idProperty));
                    });

                    Ext.each(selectedRecords, function (record) {
                        Ext.Array.push(me.multipageSelection, record.get(idProperty));
                    });
                },
                buffer: 5
            }
        });

        return selModel;
    },

    createToolBar: function () {
        var me = this;

        var toolBar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    text: 'Отметить все',
                    scale: 'small',
                    iconAlign: 'left',
                    icon: 'Scripts/ext/icons/bullet_green.png',
                    handler: me.onSelectAll,
                    scope: me
                },
                {
                    xtype: 'button',
                    text: 'Cнять все',
                    scale: 'small',
                    iconAlign: 'left',
                    icon: 'Scripts/ext/icons/bullet_red.png',
                    handler: me.clearValue,
                    scope: me
                },
            ]
        });

        return toolBar;
    },

    initComponent: function () {
        var me = this;
        me.selModel = me.createSelModel();

        if (me.showSelectAll) {
            me.dockedItems.push(me.createToolBar());
        }

        me.callParent(arguments);

        me.down('pagingtoolbar').on('change', function () {
            var records = [];
            me.getStore().data.each(function (record) {
                if (Ext.Array.contains(me.getSelectionModel().multipageSelection, record.get(me.idProperty))) {
                    records.push(record);
                }
            });
            me.getSelectionModel().select(records);
        });
    },

    /**
    * Обработка события выбора всех значений
    */
    onSelectAll: function () {
        var me = this;
        me.getSelectionModel().selectAll();
        me.fireEvent('valueselectedall', me, me.getValue());
    },

    getValue: function () {
        var me = this;
        return me.getSelectionModel().multipageSelection;
    },

    setValue: function (data) {
        var me = this;
        me.getSelectionModel().multipageSelection = data;
    },

    clearValue: function () {
        var me = this;
        me.getSelectionModel().multipageSelection = [];
        me.getSelectionModel().deselectAll(true);
    }
});