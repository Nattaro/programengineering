﻿/**
 * Создаём объект с условиями
 *
 * @param left
 * @param operand
 * @param right
 */
function CondExpr(left, operand, right) {
    "use strict";

    this.left = left;
    this.right = right;
    this.op = operand;

    //    this.display = function() {
    //        console.log(this.left + ' ' + this.operand + ' ' + this.right );
    //    };

    //    this.toString = function() {
    //        return ' {left:' + this.left + ', op: ' + this.operand + ', right:' + this.right + '} ';
    //    };
}

//---------------------------------------------------------------------//
// Операнды
CondExpr.operands = {
    and: 'and',
    or: 'or',
    eq: 'eq',
    neq: 'neq',
    gt: 'gt',
    lt: 'lt',
    gte: 'gte',
    lte: 'lte',
    contains: 'icontains',
    start: 'startswith',
    end: 'endswith'
};

CondExpr.createForTerms = function (terms, operand) {
    "use strict";

    var i,
        left = null;

    if (terms.length == 1)
        left = terms[0];
    else
        for (i = 1; i < terms.length; i += 1) {
            left = left || terms[i - 1];
            left = new CondExpr(left, operand, terms[i]);
        }
    return left;
};

//var exp = new CondExpr('name', CondExpr.operands.eq, '123123');
//exp.display();


//*****************************//
//*****************************//

/* Пример использования
var filter = CondExpr.createForTerms([
    new CondExpr('id', CondExpr.operands.gt, 5),
    new CondExpr('name', CondExpr.operands.sswith, 'Ив'),
    new CondExpr('age', CondExpr.operands.lt, 45)],
    CondExpr.operands.and
);

*/
//var ppp = new DemoRest.ConditionalExp;

//console.log(DemoRest.ConditionalExp.sswith);